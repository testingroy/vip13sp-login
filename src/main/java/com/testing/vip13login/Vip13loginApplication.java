package com.testing.vip13login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vip13loginApplication {

    public static void main(String[] args) {
        SpringApplication.run(Vip13loginApplication.class, args);
    }

}
