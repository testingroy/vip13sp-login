package com.testing.vip13login.mapper;

import com.testing.vip13login.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Classname UserMapper
 * @Description 类型说明
 * @Date 2022/9/26 20:30
 * @Created by 特斯汀Roy
 */
@Mapper
public interface UserMapper {
    @Insert("insert into userinfo values(0,#{nick},#{desc},#{uname},#{pwd})")
    int insertUser(@Param("uname") String username,
                   @Param("pwd") String password,
                   @Param("nick") String nickname,
                   @Param("desc") String describe);

    @Select("select * from userinfo where username=#{uname} and password=#{pwd}")
    User queryUserByUserNameAndPwd(@Param("uname") String username,
                                   @Param("pwd") String password);

    @Select("select * from userinfo where username=#{uname}")
    User queryUserByUserName(@Param("uname") String username);

    @Select("select * from userinfo")
    List<User> queryAllUsers();

    @Update("update userinfo set nickname=#{nick} where username=#{uname}")
    int updateNickNameByUserName(@Param("nick") String nickname,
                                 @Param("uname") String username);

    @Delete("delete from userinfo where username=#{uname}")
    int deleteByUserName(@Param("uname") String u);

}
