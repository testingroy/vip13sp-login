package com.testing.vip13login.service.impl;

import com.testing.vip13login.mapper.UserMapper;
import com.testing.vip13login.model.Result;
import com.testing.vip13login.model.User;
import com.testing.vip13login.service.UserService;
import org.apache.ibatis.io.ResolverUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Classname UserServiceImpl
 * @Description 类型说明
 * @Date 2022/9/26 22:12
 * @Created by 特斯汀Roy
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public Result login(String username, String password) {

        if (username == null || password == null)
            return new Result(-1, "用户名密码不能为空！", "用户名密码不能为空！");
        if (username.length() > 2 && username.length() < 17 &&
                password.length() > 2 && password.length() < 17) {
            User user = userMapper.queryUserByUserNameAndPwd(username, password);
            System.out.println("数据库查到的结果是" + user);
            //用户名密码如果是错的，查到的结果是null
            if (user == null) {
                return new Result(-3, "用户名或密码错误！", "用户名或密码错误！");
            } else {
                return new Result(1, "恭喜你，登录成功", user);
            }
        } else {
            return new Result(-2, "用户名密码必须是3-16位！", "用户名密码必须是3-16位！");
        }
    }

    @Override
    public Result register(String username, String password, String nickname, String describe) {
        if (username == null || password == null||nickname==null||describe==null)
            return new Result(-1, "注册失败", "各项参数不能为空！");
        if(username.length()>17||username.length()<3||password.length()>17||username.length()<3)
            return new Result(-2,"注册失败","用户名密码需要是3-16位");
        try {
            int i = userMapper.insertUser(username, password, nickname, describe);
            if(i==1){
                return new Result(1,"注册成功","注册成功");
            }else{
                return new Result(0,"注册失败","请检查注册SQL语句");
            }
        } catch (Exception e) {
            return new Result(0,"注册失败","请检查服务端日志");
        }
    }

    @Override
    public Result queryByUserName(String username) {
        Result result = new Result();
        User user = userMapper.queryUserByUserName(username);
        result.setCode(1);
        result.setMsg("查询成功");
        result.setData(user);
        return result;
    }

    @Override
    public Result deleteUserByName(String username) {
        int deleteCode = userMapper.deleteByUserName(username);
        if(deleteCode==1){
            return  new Result(1,"删除用户成功","已将"+username+"用户删除");
        }else{
            return new Result(-2,"删除用户失败","没有"+username+"用户");
        }
    }


}
