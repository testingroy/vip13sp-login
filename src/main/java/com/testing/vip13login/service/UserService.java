package com.testing.vip13login.service;

import com.testing.vip13login.model.Result;
import org.springframework.stereotype.Service;

/**
 * @Classname UserService
 * @Description 类型说明
 * @Date 2022/9/26 22:04
 * @Created by 特斯汀Roy
 */

public interface UserService {
    //登录
    Result login(String username,String password);
    //注册
    Result register(String username,String password,String nickname,String describe);

    Result queryByUserName(String username);

    Result deleteUserByName(String username);
}
