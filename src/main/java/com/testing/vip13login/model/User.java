package com.testing.vip13login.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname User
 * @Description 类型说明
 * @Date 2022/9/26 20:08
 * @Created by 特斯汀Roy
 */
@Data
//加上一个全参的构造方法。
@AllArgsConstructor
//无参
@NoArgsConstructor
public class User {
    public int id;
    public String username;
    public String password;
    public String nickname;
    public String describe;

    //全参构造方法
//    public User(int id ,String username,String password,String nickname,String description){
//        this.id=id;
//        this.username=username;
//    }

}
