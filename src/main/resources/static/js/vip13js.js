function onLi(element) {
    element.style.backgroundColor = 'gray';
    element.style.color = 'white';
}

function leaveLi(element) {
    element.style.backgroundColor = 'white';
    element.style.color = 'black';
}


function ajaxLogin() {
    $.ajax({
        url: "user/login",
        type: "post",
        //请求数据，将form表单中的数据序列化成为一个urlencoded格式  键=值&键=值的请求。
        data: $("form").serialize(),
        //ajax在success中定义的方法，默认的参数就有请求的返回结果
        success: function (result) {
            console.log(result);
            //js中的对象解析：
            //1、 对象.键   2、对象["键"]
            $("h1")[0].innerHTML = result.msg;
            //判断如果成功就跳转
            if (result.code == 1) {
                //跳转到指定url
                window.location.href = "user.html";
            }
        },
        error: function () {
            alert("登录接口请求失败了，请抓包并检查服务端日志！")
        }
    })
}

function getUserInfo(){
    $.ajax({
        url:"user/getUser",
        type:"get",
        success:function (result) {
            $("#userid")[0].innerHTML=result.data.id;
            $("#nickname")[0].innerHTML=result.data.nickname;
            $("#describe")[0].innerHTML=result.data.describe;
        },
        error:function () {
            alert("用户信息接口请求失败了，请抓包并检查服务端日志！")
        }
    })
}

function logout() {
    $.ajax({
        url:"user/logout",
        type:"get",
        success:function () {
            window.location.href="index.html";
        }
    })
}

function delUser() {
    const user=$("#delName")[0].value;
    $.ajax({
        url:"user/delete/"+user,
        type:"delete",
        dataType:"json",
        success:function (result) {
            $("#delInfo")[0].innerText=result.msg;

        },
        error:function () {
            alert("删除接口调用失败，请检查浏览器console以及抓包信息");
        }
    })
}

function ajaxRegister() {
    const user=$("#username")[0].value;
    const pwd=$("#password")[0].value;
    const nickname=$("#nickname")[0].value;
    const desc=$("#describe")[0].value;
    $.ajax(
        {
            url:"user/register",
            type:"post",
            // dataType:"json",
            contentType:"application/json",
            data:JSON.stringify({"username":user,"password":pwd,"nickname":nickname,"describe":desc}),
            // data:$('#info').serialize(),
            success:function (result) {
                if(result.code==1){
                    window.location.href="index.html";
                }else{
                    $("#regInfo")[0].innerText=result.msg;
                }
            },
            error:function () {
                $("#regInfo")[0].innerText="注册接口调用失败，请抓包检查！";
            }
        }
    )
}