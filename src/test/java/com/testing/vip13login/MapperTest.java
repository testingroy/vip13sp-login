package com.testing.vip13login;

import com.testing.vip13login.mapper.UserMapper;
import com.testing.vip13login.model.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

/**
 * @Classname MapperTest
 * @Description 类型说明
 * @Date 2022/9/26 21:34
 * @Created by 特斯汀Roy
 */
@Transactional
@Rollback
@RunWith(SpringRunner.class)
@SpringBootTest
public class MapperTest {

    @Autowired
    UserMapper userMapper;

    @Test
    public void testMapper(){
        int result = userMapper.updateNickNameByUserName("天花板", "地板");
        System.out.println("正确的更新操作的结果是"+result);

        int falseResult=userMapper.updateNickNameByUserName("哆啦A梦","机器猫");
        System.out.println("错误的更新操作的结果是"+falseResult);
    }

    @Test
    public void testDel(){
        int fault = userMapper.deleteByUserName("哆啦A梦");
        System.out.println(fault);
//        Assert.assertTrue(fault==1);
    }

    @Test
    public void testInsert(){
        //生成一个100000以内的随机数
        Random random=new Random();
        int nextInt = random.nextInt(100000);
        int user = userMapper.insertUser("roy" + nextInt, "123456", "royrou", "老师");
        System.out.println("插入了roy"+nextInt);
        System.out.println(user);
    }

}
