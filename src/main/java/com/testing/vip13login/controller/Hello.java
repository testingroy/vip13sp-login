package com.testing.vip13login.controller;

import com.testing.vip13login.mapper.UserMapper;
import com.testing.vip13login.model.Result;
import com.testing.vip13login.model.User;
import com.testing.vip13login.outer.LoginSample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javafx.application.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Classname Hello
 * @Description 类型说明
 * @Date 2022/9/16 20:52
 * @Created by 特斯汀Roy
 */

//构建一个restful风格的接口测试类。
@RestController
//映射到url  项目/hello  其中的所有接口，以/hello的路径开始。
@RequestMapping("/hello")
@Api(description = "欢迎使用springboot接口",tags = "欢迎系列")
public class Hello {

    //自动装配的接口类
    @Autowired
    UserMapper userMapper;

    //这是一个get请求的映射,produces用来指定返回的content-type
    @GetMapping(produces = "application/json")
    public String hello(){
        return "hello spring!";
    }

    @GetMapping("/visit")
    public String helloName(@RequestParam(required = false) String name){
        if(name==null){
            return "不好意思，请输入名字";
        }
        if("roy".equals(name)){
            return "欢迎回家"+name;
        }else{
            return "尊敬的"+name+"你好，请联系Roy预约访问";
        }
    }

    //路径变量，在路径中用{变量名}引用，需要和参数列表的变量名一致。
    @GetMapping("/guest/{name}")
    public String welcome(@PathVariable String name){
        return "欢迎光临"+name;
    }

    @ApiOperation( value="登录",notes="这是个登录请求")
    @PostMapping("/login/{age}")
    public String Login( @ApiParam(value="用户名",defaultValue = "Roy") @RequestParam(required = false) String username,String password,
                        @ApiParam(value="年龄",defaultValue = "32") @PathVariable int age){
//        password.contains("123456");
        System.out.println("密码是"+password);
        LoginSample ls=new LoginSample();
        ls.login(username, password);
        return ls.getResultMSG()+"年龄是"+age;
    }

    //使用controller直接操作Mapper
    @PostMapping("/login/add")
    public String addUser(String username,String password,String nickname,String describe){
        int result = userMapper.insertUser(username, password, nickname, describe);
        if(result==1){
            return "插入成功";
        }else{
            return "插入失败";
        }
    }

//    @requestBody 可以对一个json对象处理为java中的对象格式来进行使用。
    @PostMapping("/login/addUser")
    public String addJsonUSer(@RequestBody User user){
        System.out.println(user);
        int i = userMapper.insertUser(user.getUsername(), user.getPassword(), user.getNickname(), user.getDescribe());
        if(i==1){
            return "插入成功";
        }else{
            return "插入失败";
        }
    }


    @GetMapping("/queryUser")
    public User queryUser(String u,String p){
        System.out.println(u+p);
        User user = userMapper.queryUserByUserNameAndPwd(u, p);
        System.out.println(user);
        if(user==null){
            return new User(-1,"没这个用户","登录失败","可拉倒吧","自己注册去");
        }
        return user;
    }


    @PutMapping("/updateUser")
    public Result updateUser(String username,String nickname){
        int i = userMapper.updateNickNameByUserName(nickname, username);
        if(i==1){
            return new Result(200,"更新用户成功","");
        }else{
            return new Result(200,"更新用户失败","");
        }

    }

    @DeleteMapping("/deleteUser/{username}")
    public Result deleteUser(@PathVariable String username){
        int i = userMapper.deleteByUserName(username);
        if(i==1){
            return new Result(200,"删除用户成功","");
        }else{
            return new Result(200,"删除用户失败","");
        }
    }


}
