package com.testing.vip13login.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname Result
 * @Description 类型说明
 * @Date 2022/9/26 22:07
 * @Created by 特斯汀Roy
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    public int code;
    public String msg;
    public Object data;
}
