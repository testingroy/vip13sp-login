package com.testing.vip13login;

import com.testing.vip13login.model.Result;
import com.testing.vip13login.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Classname ServiceTest
 * @Description 类型说明
 * @Date 2022/9/26 22:27
 * @Created by 特斯汀Roy
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ServiceTest {
    @Autowired
    UserService userService;

    @Test
    public void testlogin(){
        Result roy = userService.login("roy", "123456");
        System.out.println(roy);
    }

}
