package com.testing.vip13login;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @Classname Swagger3Conf
 * @Description 对Swagger页面进行设置修改
 * @Date 2022/5/18 22:50
 * @Created by 特斯汀Roy
 */
//注解标注这个类是个配置类，需要在运行时加载。
@Configuration
public class Swagger3Configuration {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                // 将api的元信息设置为包含在json ResourceListing响应中。
                .apiInfo(apiInfo())
                // 选择哪些接口作为swagger的doc发布
                .select()
                //去掉basic-error-controller的显示
                .paths(PathSelectors.regex(".*error").negate())
                .build();
    }

    /**
     * API 页面上半部分展示信息
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("特斯汀学院接口文档")
                .description("Roy出品")
                .contact(new Contact("Roy", "http://www.testingedu.com.cn", "578225840@qq.com"))
                .version("1.0.0")
                .build();
    }
}

